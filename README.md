# Crio Byte: Abstraction

Understand the Object Oriented concept of Abstraction
1. Learn where and why Abstraction is applicable
2. Understand how to apply Abstraction

## Getting Started
You can download the source code from gitlab by executing one of the following commands:
```
git clone https://gitlab.crio.do/crio_bytes/me_abstraction.git
git clone git@gitlab.crio.do:crio_bytes/me_abstraction.git
```

If you don’t have Git already installed. Use [this](https://www.linode.com/docs/development/version-control/how-to-install-git-on-linux-mac-and-windows/) as a reference to help yourselves do that.


## Project Files

    .
    ├── base_code  /                  
    ├── base_code_with_xml_support    /                  
    ├── oop_way /             
    └── README.md
